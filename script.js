
// 3. Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)

const getCube = 8 ** 3
console.log(getCube);

// 4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…

console.log(`The cube of 8 is ${getCube}`)

// 5. Create a variable address with a value of an array containing details of an address.

const address = ["258 Washington Ave NW", "California", "90011"];

// 6. Destructure the array and print out a message with the full address using Template Literals.
const [streetName, state, zipCode] = address;
console.log(`I live at ${streetName} ${state} ${zipCode}!!`);

// 7. Create a variable animal with a value of an object data type with different animal details as it’s properties.
const animal = {
	species: "saltwater crocodile",
	weight: "1075kgs",
	length: "20ft 3in"
}

// 8. Destructure the object and print out a message with the details of the animal using Template Literals.
const {species, weight, length} = animal;


function lolong ({species, weight, length}){
	console.log(`Lolong was a ${species}. He weighed at ${weight} with a measurement of ${length}`);
}


lolong(animal);

// 9. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

// 10. Create/instantiate a new object from the class Dog and console log the object.

const myDog = new Dog("Churro", "5 years", "Rottweiler");
console.log(myDog);